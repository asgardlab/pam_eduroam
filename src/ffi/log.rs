use syslog;
pub use syslog::Severity;

pub fn try_log(severity: Severity, msg: &str) {
    match syslog::unix(syslog::Facility::LOG_AUTHPRIV) {
        Err(_) => {}
        Ok(writer) => {
            let _ = writer.send_3164(severity, msg);
        }
    }
}
