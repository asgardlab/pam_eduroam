use std;
use libc;
use errors::{Result, ErrorKind};

pub fn not_null<T>(ptr: *const T) -> Result<*const T> {
    if ptr.is_null() {
        Err(ErrorKind::NullPointer.into())
    } else {
        Ok(ptr)
    }
}

pub fn not_null_mut<T>(ptr: *mut T) -> Result<*mut T> {
    if ptr.is_null() {
        Err(ErrorKind::NullPointer.into())
    } else {
        Ok(ptr)
    }
}

pub fn ptr_to_str(ptr: *const libc::c_char) -> Result<String> {
    let cstr = unsafe { std::ffi::CStr::from_ptr(not_null(ptr)?) };
    let bytes = cstr.to_bytes();
    Ok(std::str::from_utf8(bytes)?.to_owned())
}
