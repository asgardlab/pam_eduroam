use std;
use libc;
use ffi::log::*;
use core;
use pam::*;
use errors::{Result, ErrorKind, Error};

fn err_wrapper<F>(f: F) -> libc::c_int
    where F: FnOnce() -> Result<()> + std::panic::UnwindSafe
{
    let res = match std::panic::catch_unwind(f) {
        Ok(Ok(())) => ResultCode::Success,
        Ok(Err(Error(ErrorKind::PamError(res_code), _))) => {
            try_log(Severity::LOG_ERR,
                    &format!("pam_eduroam: result: {:?}", res_code));
            match res_code {
                // Success does not make sense in an error, return something else
                ResultCode::Success => ResultCode::ServiceErr,
                res_code => res_code,
            }
        }
        Ok(Err(ref err)) => {
            try_log(Severity::LOG_ERR, &format!("pam_eduroam: error: {}", err));
            ResultCode::ServiceErr
        }
        Err(ref err) => {
            try_log(Severity::LOG_CRIT,
                    &format!("pam_eduroam: unwind error: {:?}", err));
            ResultCode::ServiceErr
        }
    };
    res as libc::c_int
}

#[no_mangle]
pub extern "C" fn pam_sm_authenticate(pamh: &PamHandleT,
                                      _flags: libc::c_int,
                                      _argc: libc::c_int,
                                      _argv: *const *const libc::c_char)
                                      -> libc::c_int {
    err_wrapper(|| core::authenticate(pamh))
}

#[no_mangle]
pub extern "C" fn pam_sm_setcred(_pamh: &PamHandleT,
                                 _flags: libc::c_int,
                                 _argc: libc::c_int,
                                 _argv: *const *const libc::c_char)
                                 -> libc::c_int {
    ResultCode::Success as libc::c_int
}

#[no_mangle]
pub extern "C" fn pam_sm_open_session(_pamh: &PamHandleT,
                                      _flags: libc::c_int,
                                      _argc: libc::c_int,
                                      _argv: *const *const libc::c_char)
                                      -> libc::c_int {
    ResultCode::Success as libc::c_int
}

#[no_mangle]
pub extern "C" fn pam_sm_close_session(_pamh: &PamHandleT,
                                       _flags: libc::c_int,
                                       _argc: libc::c_int,
                                       _argv: *const *const libc::c_char)
                                       -> libc::c_int {
    err_wrapper(|| core::close_session())
}
