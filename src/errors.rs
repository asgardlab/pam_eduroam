use std::{ffi, str, io};
use dbus;
use rusqlite;
use toml;
use pam;

error_chain! {
    foreign_links {
        NulError(ffi::NulError);
        Utf8Error(str::Utf8Error);
        DBusError(dbus::Error);
        SqliteError(rusqlite::Error);
        IoError(io::Error);
        TomlError(toml::de::Error);
    }
    errors {
        AccountNotFound {
            description("account not found")
            display("Account does not exist in the database")
        }
        DBusPropertyReadError(property: &'static str) {
            description("cannot read dbus property")
            display("Cannot read dbus property '{}'", property)
        }
        NetworkDeviceNotFound {
            description("network device not found")
            display("Network device not found")
        }
        PamError(res_code: pam::ResultCode) {
            description("pam library call failure")
            display("PAM library call failed with code {:?}", res_code)
        }
        RealmNotFound(username: String) {
            description("realm not found")
            display("supplied user '{}' has no realm", username)
        }
        UnknownRealm(realm: String) {
            description("unknown realm")
            display("Security settings are not known for realm '{}'", realm)
        }
        ConnectionTimeout {
            description("connection timeout")
            display("Connection has timed out")
        }
        ConnectionDoesNotMatch {
            description("Connection does not match")
            display("Active connection does not match the one we created")
        }
        TooManyUsers {
            description("too many users")
            display("Too many users were registered in this system")
        }
        OutOfMemory {
            description("out of memory")
            display("The buffer is out of memory")
        }
        NullPointer {
            description("null pointer")
            display("Received a null pointer")
        }
    }
}
