use std::ffi::CString;
use dbus::arg::{cast, RefArg, Variant};
use uuid::Uuid;
use pam::*;
use nmclient::{NMClient, State, DeviceType, ConnectionSettings};
use userdb::UserDB;
use settings::{Settings, EAPConfig};
use errors::{Result, ErrorKind};
use itertools::Itertools;

macro_rules! s {
    ($s:expr) => (String::from($s));
}
macro_rules! v {
    ($v:expr) => (Variant(Box::new($v) as Box<RefArg>));
}

pub fn authenticate(pamh: &PamHandleT) -> Result<()> {
    let settings = Settings::load()?;
    let nm = NMClient::connect()?;

    let username = settings.insert_realm(pamh.get_user()?);
    let password = pamh.get_password()?;
    let eapconfig = eapconfig(&settings, &username)?;

    nm.enable_wifi(v!(true))?;

    // Apparently we need to call this everytime, since we cannot clone the RefArgs
    let connection = || mk_connection(&settings, &eapconfig, &username, &password);

    // Skip creating connection if already valid, active and its user/password matches
    if validate_connection(&nm, connection()?).is_ok() {
        return Ok(());
    }

    del_connections(&nm, &settings.wireless.ssid)?;

    let device_path =
        nm.device_path(DeviceType::Wifi,
                         settings.wireless.device.as_ref().map(String::as_ref))?;
    nm.add_and_activate_connection(connection()?, device_path)?;

    wait_for_connection(&nm, settings.wireless.timeout)?;
    // Check whether the connection which has been established was really created by ouserlves,
    // and not by the user.
    validate_connection(&nm, connection()?)?;

    UserDB::open(&settings)?
        .get_or_create_uid(&username)
        .map(|_uid| ())
}

pub fn close_session() -> Result<()> {
    let settings = Settings::load()?;
    let nm = NMClient::connect()?;
    del_connections(&nm, &settings.wireless.ssid)
}

fn mk_connection(settings: &Settings,
                 eapconfig: &EAPConfig,
                 username: &str,
                 password: &str)
                 -> Result<ConnectionSettings> {
    Ok(hashmap!{
        s!("connection") => hashmap!{
            s!("type") => v!(s!("802-11-wireless")),
            s!("uuid") => v!(Uuid::new_v4().hyphenated().to_string()),
            s!("permissions") => v!(vec![format!("user:{}", username)]),
            s!("id") => v!(settings.wireless.ssid.clone()),
        },
        s!("802-11-wireless") => hashmap!{
            s!("ssid") => v!(settings.wireless.ssid.clone().into_bytes()),
            s!("security") => v!(s!("802-11-wireless-security")),
        },
        s!("802-11-wireless-security") => hashmap!{
            s!("key-mgmt") => v!(s!("wpa-eap")),
            s!("proto") => v!(vec![s!("rsn")]),
            s!("pairwise") => v!(vec![s!("ccmp")]),
            s!("group") => v!(vec![s!("ccmp"), s!("tkip")]),
        },
        s!("802-1x") => hashmap!{
            s!("identity") => v!(s!(username)),
            s!("password") => v!(s!(password)),
            s!("eap") => v!(eapconfig.eap.clone()),
            s!("phase2-auth") => v!(eapconfig.phase2_auth.clone()),
            s!("ca-cert") => v!(CString::new(format!("file://{}/{}",
                                                   settings.path.ca_cert_dir,
                                                   eapconfig.ca_cert))?
                                      .into_bytes_with_nul()),
            s!("altsubject-matches") => v!(vec![format!("DNS:{}", eapconfig.server_name)]),
            s!("anonymous-identity") => v!(eapconfig.anonymous_identity.clone()),
        },
        s!("ipv4") => hashmap!{
            s!("method") => v!(s!("auto")),
        },
        s!("ipv6") => hashmap!{
            s!("method") => v!(s!("auto")),
        },
    })
}

macro_rules! r {
    ($dic:expr, $k1:expr, $k2:expr, $T:ty) => (
        $dic.get($k1).and_then(|d| d.get($k2)).and_then(|v| cast(&v.0) as Option<&$T>)
    );
}
macro_rules! check {
    ($dic1:expr, $dic2:expr, $k1:expr, $k2:expr, $T:ty) => {
        let v1 = r!($dic1, $k1, $k2, $T);
        let v2 = r!($dic2, $k1, $k2, $T);
        if v1.is_none() || v1 != v2 {
            return Err(ErrorKind::ConnectionDoesNotMatch.into());
        }
    };
}

fn validate_connection(nm: &NMClient, expected: ConnectionSettings) -> Result<()> {
    let connection = nm.primary_connection()?;
    let actual = connection.get_settings()?;
    let secret = connection.get_secrets("802-1x")?;

    check!(expected, actual, "connection", "type", String);
    check!(expected, actual, "802-11-wireless", "ssid", Vec<u8>);
    check!(expected, actual, "802-11-wireless", "security", String);
    check!(expected, actual, "802-11-wireless-security", "key-mgmt", String);
    check!(expected, actual, "802-11-wireless-security", "proto", Vec<String>);
    check!(expected, actual, "802-11-wireless-security", "pairwise", Vec<String>);
    check!(expected, actual, "802-11-wireless-security", "group", Vec<String>);
    check!(expected, actual, "802-1x", "identity", String);
    check!(expected, secret, "802-1x", "password", String);
    check!(expected, actual, "802-1x", "eap", Vec<String>);
    check!(expected, actual, "802-1x", "phase2-auth", String);
    check!(expected, actual, "802-1x", "ca-cert", Vec<u8>);
    check!(expected, actual, "802-1x", "altsubject-matches", Vec<String>);
    check!(expected, actual, "802-1x", "anonymous-identity", String);

    Ok(())
}

fn del_connections(nm: &NMClient, target_ssid: &str) -> Result<()> {
    for connection in nm.list_connections()? {
        let settings = connection.get_settings()?;
        if let Some(ssid) =
            r!(settings, "802-11-wireless", "ssid", Vec<u8>).map(|s| String::from_utf8_lossy(s)) {
            if ssid == target_ssid {
                connection.delete()?;
            }
        }
    }
    Ok(())
}

fn wait_for_connection(nm: &NMClient, seconds: u32) -> Result<()> {
    let monitor = nm.monitor_state()?;
    monitor
        .wait(seconds, |s| {
            s == State::ConnectedLocal || s == State::ConnectedSite || s == State::ConnectedGlobal
        })
        .map(|_s| ())
        .ok_or_else(|| ErrorKind::ConnectionTimeout.into())
}

fn eapconfig(settings: &Settings, username: &str) -> Result<EAPConfig> {
    let (realm, _) = split_realm(&username)?;
    settings
        .eap_map()?
        .get(realm)
        .map(|cfg| cfg.clone())
        .ok_or_else(|| ErrorKind::UnknownRealm(String::from(realm)).into())
}

fn split_realm(username: &str) -> Result<(&str, &str)> {
    username
        .rsplitn(2, '@')
        .tuples()
        .next()
        .ok_or_else(|| ErrorKind::RealmNotFound(String::from(username)).into())
}
