#![recursion_limit="128"]
#[macro_use]
extern crate error_chain;
extern crate dbus;
#[macro_use]
extern crate dbus_macros;
#[macro_use]
extern crate enum_primitive;
extern crate num_traits;
#[macro_use]
extern crate maplit;
extern crate uuid;
extern crate rusqlite;
extern crate syslog;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate toml;
extern crate itertools;
extern crate libc;

mod core;
mod settings;
mod nmclient;
mod userdb;
mod pam;
mod ffi;
mod extern_nss;
mod extern_pam;
mod errors;

pub use extern_nss::*;
pub use extern_pam::*;
