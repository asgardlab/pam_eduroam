use std;
use libc;
use ffi::ptr::*;
use ffi::log::*;
use userdb::*;
use settings::Settings;
use errors::{Result, ErrorKind, Error};

#[allow(dead_code)]
#[derive(Debug, PartialEq)]
enum Status {
    TryAgain = -2,
    Unavail = -1,
    NotFound = 0,
    Success = 1,
}

#[repr(C)]
pub struct Passwd {
    name: *mut libc::c_char,
    passwd: *mut libc::c_char,
    uid: Uid,
    gid: libc::gid_t,
    gecos: *mut libc::c_char,
    dir: *mut libc::c_char,
    shell: *mut libc::c_char,
}

#[repr(C)]
pub struct Group {
    name: *mut libc::c_char,
    passwd: *mut libc::c_char,
    gid: libc::gid_t,
    mem: *mut *mut libc::c_char,
}


struct StaticBuffer {
    buf: *mut libc::c_char,
    buflen: libc::size_t,
}

impl StaticBuffer {
    fn new(buf: *mut libc::c_char, buflen: libc::size_t) -> Result<Self> {
        Ok(StaticBuffer {
               buf: not_null_mut(buf)?,
               buflen: buflen,
           })
    }

    fn alloc(&mut self, count: usize) -> Result<*mut libc::c_char> {
        let offset = count as isize;
        if offset < 0 || self.buflen < offset as usize {
            return Err(ErrorKind::OutOfMemory.into());
        }
        let res = self.buf;
        unsafe {
            self.buf = res.offset(offset);
        }
        self.buflen -= offset as usize;
        Ok(res)
    }

    fn alloc_ptrvec(&mut self, count: usize) -> Result<*mut *mut libc::c_char> {
        let ptr: *const libc::c_char = std::ptr::null();
        unsafe {
            let res = self.alloc(count * std::mem::size_of_val(&ptr))?;
            Ok(std::mem::transmute(res))
        }
    }

    fn copy_str(&mut self, s: &str) -> Result<*mut libc::c_char> {
        let len = s.len() + 1;
        let res = self.alloc(len)?;
        let cstr = std::ffi::CString::new(s)?;
        unsafe {
            std::ptr::copy(cstr.as_ptr(), res, len);
        }
        Ok(res)
    }
}


trait Fillable {
    fn fill(&mut self,
            settings: &Settings,
            buf: &mut StaticBuffer,
            account: &str,
            uid: Uid)
            -> Result<()>;
}

impl Fillable for Passwd {
    fn fill(&mut self,
            settings: &Settings,
            buf: &mut StaticBuffer,
            account: &str,
            uid: Uid)
            -> Result<()> {
        self.name = buf.copy_str(account)?;
        self.passwd = buf.copy_str("!")?;
        self.uid = uid;
        self.gid = uid;
        self.gecos = buf.copy_str(account)?;
        self.dir = buf.copy_str(&format!("{}/{}", &settings.user.home_dir, account))?;
        self.shell = buf.copy_str(&settings.user.shell)?;
        Ok(())
    }
}

impl Fillable for Group {
    fn fill(&mut self,
            _settings: &Settings,
            buf: &mut StaticBuffer,
            account: &str,
            uid: Uid)
            -> Result<()> {
        self.name = buf.copy_str(account)?;
        self.passwd = buf.copy_str("!")?;
        self.gid = uid;
        self.mem = buf.alloc_ptrvec(1)?;
        unsafe {
            *self.mem = std::ptr::null_mut();
        }
        Ok(())
    }
}

fn from_uid<T>(uid: Uid, st_ptr: *mut T, buf: *mut libc::c_char, buflen: libc::size_t) -> Result<()>
    where T: Fillable
{
    let settings = Settings::load()?;
    let db = UserDB::open(&settings)?;
    let account = db.get_account(uid)?;
    unsafe {
        let ref mut st = *not_null_mut(st_ptr)?;
        st.fill(&settings,
                  &mut StaticBuffer::new(buf, buflen)?,
                  &account,
                  uid)?;
    }
    Ok(())
}

fn from_nam<T>(name: *const libc::c_char,
               st_ptr: *mut T,
               buf: *mut libc::c_char,
               buflen: libc::size_t)
               -> Result<()>
    where T: Fillable
{
    let settings = Settings::load()?;
    let db = UserDB::open(&settings)?;
    let account = settings.insert_realm(ptr_to_str(name)?);
    let uid = db.get_uid(&account)?;
    unsafe {
        let ref mut st = *not_null_mut(st_ptr)?;
        st.fill(&settings,
                  &mut StaticBuffer::new(buf, buflen)?,
                  &account,
                  uid)?;
    }
    Ok(())
}


fn err_wrapper<F>(f: F, errnop: *mut libc::c_int) -> libc::c_int
    where F: FnOnce() -> Result<()> + std::panic::UnwindSafe
{
    let (res, errno) = match std::panic::catch_unwind(f) {
        Ok(Ok(())) => (Status::Success, 0),
        Ok(Err(Error(ErrorKind::OutOfMemory, _))) => (Status::TryAgain, libc::ERANGE),
        Ok(Err(Error(ErrorKind::AccountNotFound, _))) => (Status::NotFound, libc::ERANGE),
        Ok(Err(ref err)) => {
            try_log(Severity::LOG_ERR, &format!("nss_eduroam: error: {}", err));
            (Status::Unavail, libc::ENOENT)
        }
        Err(ref err) => {
            try_log(Severity::LOG_CRIT,
                    &format!("nss_eduroam: unwind error: {:?}", err));
            (Status::Unavail, libc::ENOENT)
        }
    };
    if !errnop.is_null() {
        unsafe {
            *errnop = errno;
        }
    }
    res as libc::c_int
}


#[no_mangle]
pub extern "C" fn _nss_eduroam_setpwent() -> libc::c_int {
    Status::Success as libc::c_int
}

#[no_mangle]
pub extern "C" fn _nss_eduroam_endpwent() -> libc::c_int {
    Status::Success as libc::c_int
}

#[no_mangle]
pub extern "C" fn _nss_eduroam_getpwent_r() -> libc::c_int {
    Status::Unavail as libc::c_int
}

#[no_mangle]
pub extern "C" fn _nss_eduroam_getpwuid_r(uid: Uid,
                                          pwd: *mut Passwd,
                                          buf: *mut libc::c_char,
                                          buflen: libc::size_t,
                                          errnop: *mut libc::c_int)
                                          -> libc::c_int {
    err_wrapper(|| from_uid(uid, pwd, buf, buflen), errnop)
}

#[no_mangle]
pub extern "C" fn _nss_eduroam_getpwnam_r(name: *const libc::c_char,
                                          pwd: *mut Passwd,
                                          buf: *mut libc::c_char,
                                          buflen: libc::size_t,
                                          errnop: *mut libc::c_int)
                                          -> libc::c_int {
    err_wrapper(|| from_nam(name, pwd, buf, buflen), errnop)
}


#[no_mangle]
pub extern "C" fn _nss_eduroam_setgrent() -> libc::c_int {
    Status::Success as libc::c_int
}

#[no_mangle]
pub extern "C" fn _nss_eduroam_endgrent() -> libc::c_int {
    Status::Success as libc::c_int
}

#[no_mangle]
pub extern "C" fn _nss_eduroam_getgrent_r() -> libc::c_int {
    Status::Unavail as libc::c_int
}

#[no_mangle]
pub extern "C" fn _nss_eduroam_getgrgid_r(gid: libc::gid_t,
                                          grp: *mut Group,
                                          buf: *mut libc::c_char,
                                          buflen: libc::size_t,
                                          errnop: *mut libc::c_int)
                                          -> libc::c_int {
    err_wrapper(|| from_uid(gid, grp, buf, buflen), errnop)
}

#[no_mangle]
pub extern "C" fn _nss_eduroam_getgrnam_r(name: *const libc::c_char,
                                          grp: *mut Group,
                                          buf: *mut libc::c_char,
                                          buflen: libc::size_t,
                                          errnop: *mut libc::c_int)
                                          -> libc::c_int {
    err_wrapper(|| from_nam(name, grp, buf, buflen), errnop)
}
