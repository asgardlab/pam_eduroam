extern crate libsqlite3_sys as ffi;

use rusqlite::{self, params, Connection, Error};
use settings::Settings;
use errors::{self, Result, ErrorKind};

pub use libc::uid_t as Uid;

pub struct UserDB {
    conn: Connection,
    first_uid: Uid,
}

impl UserDB {
    pub fn open(settings: &Settings) -> Result<Self> {
        let mut db = UserDB {
            conn: Connection::open(&settings.path.userdb)?,
            first_uid: settings.user.first_uid,
        };
        match db.initialize() {
            Ok(()) => Ok(db),
            Err(Error::SqliteFailure(ffi::Error {
                                         code: ffi::ErrorCode::ReadOnly, ..
                                     },
                                     ..)) => Ok(db),
            Err(err) => Err(err.into()),
        }
    }

    fn initialize(&mut self) -> rusqlite::Result<()> {
        let tx = self.conn.transaction()?;
        tx.execute("CREATE TABLE IF NOT EXISTS user (uid INTEGER PRIMARY KEY AUTOINCREMENT, \
                        account TEXT UNIQUE)",
                   params![])?;
        // An INSERT INTO OR IGNORE statement would not work because sqlite_sequence has
        // no UNIQUE constraint for name.
        tx.execute("INSERT INTO sqlite_sequence (name, seq) SELECT ?1, ?2 \
                        WHERE NOT EXISTS (SELECT 1 FROM sqlite_sequence WHERE name = ?1)",
                   params!["user", self.first_uid])?;
        tx.commit()
    }

    pub fn get_uid(&self, account: &str) -> Result<Uid> {
        let uid = self.conn
            .query_row("SELECT uid FROM user WHERE account = ?1",
                       &[&account],
                       |row| row.get(0))
            .map_err(UserDB::map_not_found)?;
        if uid <= self.first_uid {
            return Err(ErrorKind::TooManyUsers.into());
        }
        Ok(uid)
    }

    pub fn get_or_create_uid(&mut self, account: &str) -> Result<Uid> {
        let res = self.conn
            .execute("INSERT OR FAIL INTO user (account) VALUES (?1)",
                     params![account]);
        match res {
            Ok(_) => {}
            // An INSERT OR IGNORE statement would increment sqlite_sequence even if no records
            // were added. So we need to INSERT OR FAIL and ignore the resulting error here.
            Err(Error::SqliteFailure(ffi::Error {
                                         code: ffi::ErrorCode::ConstraintViolation, ..
                                     },
                                     ..)) => {}
            // If database is readonly, let get_uid fail if account does not already exist
            Err(Error::SqliteFailure(ffi::Error {
                                         code: ffi::ErrorCode::ReadOnly, ..
                                     },
                                     ..)) => {}
            Err(err) => return Err(err.into()),
        }
        self.get_uid(account)
    }

    pub fn get_account(&self, uid: Uid) -> Result<String> {
        let account = self.conn
            .query_row("SELECT account FROM user WHERE uid = ?1",
                       &[&uid],
                       |row| row.get(0))
            .map_err(UserDB::map_not_found)?;
        Ok(account)
    }

    fn map_not_found(err: Error) -> errors::Error {
        match err {
            Error::QueryReturnedNoRows => ErrorKind::AccountNotFound.into(),
            e @ _ => e.into(),
        }
    }
}
