use std::rc::Rc;
use std::collections::HashMap;
use std::time::{Instant, Duration};
use std::thread;
use dbus::{self, Path};
use dbus::arg::RefArg;
use num_traits::FromPrimitive;
use errors;


const SVC_NETWORK_MANAGER: &str = "org.freedesktop.NetworkManager";

const IFC_PROPERTIES: &str = "org.freedesktop.DBus.Properties";
const IFC_NETWORK_MANAGER: &str = "org.freedesktop.NetworkManager";
const IFC_SETTINGS: &str = "org.freedesktop.NetworkManager.Settings";
const IFC_CONNECTION: &str = "org.freedesktop.NetworkManager.Settings.Connection";
const IFC_CONNECTION_ACTIVE: &str = "org.freedesktop.NetworkManager.Connection.Active";
const IFC_DEVICE: &str = "org.freedesktop.NetworkManager.Device";

const PATH_NETWORK_MANAGER: &str = "/org/freedesktop/NetworkManager";
const PATH_SETTINGS: &str = "/org/freedesktop/NetworkManager/Settings";

const PROP_PRIMARY_CONNECTION: &str = "PrimaryConnection";
const PROP_CONNECTION: &str = "Connection";
const PROP_INTERFACE: &str = "Interface";
const PROP_DEVICE_TYPE: &str = "DeviceType";
const PROP_WIRELESS_ENABLED: &str = "WirelessEnabled";

const SIG_STATE_CHANGED: &str = "StateChanged";


enum_from_primitive! {
#[allow(dead_code)]
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum State {
    Unknown = 0,
    Asleep = 10,
    Disconnected = 20,
    Disconnecting = 30,
    Connecting = 40,
    ConnectedLocal = 50,
    ConnectedSite = 60,
    ConnectedGlobal = 70,
}
}

enum_from_primitive! {
#[allow(dead_code)]
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum DeviceType {
    Unknown = 0,
    Generic = 14,
    Ethernet = 1,
    Wifi = 2,
    Bt = 5,
    OlpcMesh = 6,
    Wimax = 7,
    Modem = 8,
    Infiniband = 9,
    Bond = 10,
    Vlan = 11,
    Adsl = 12,
    Bridge = 13,
    Team = 15,
    Tun = 16,
    IpTunnel = 17,
    Macvlan = 18,
    Vxlan = 19,
    Veth = 20,
    Macsec = 21,
    Dummy = 22,
}
}


pub type Variant = dbus::arg::Variant<Box<RefArg>>;
pub type ConnectionSettings = HashMap<String, HashMap<String, Variant>>;


dbus_interface!(IFC_PROPERTIES, interface Properties {
    fn get(interface_name: &str, property_value: &str) -> Variant;
    fn set(interface_name: &str, property_value: &str, value: Variant);
});

dbus_interface!(IFC_NETWORK_MANAGER, interface NetworkManager {
    fn get_devices() -> Vec<Path>;
    fn activate_connection(connection: Path, device: Path, specific_object: Path) -> Path;
});

dbus_interface!(IFC_SETTINGS, interface Settings {
    fn list_connections() -> Vec<Path>;
    fn add_connection(connection: ConnectionSettings) -> Path;
});

dbus_interface!(IFC_CONNECTION, interface Connection {
    fn get_settings() -> ConnectionSettings;
    fn get_secrets(setting_name: &str) -> ConnectionSettings;
    fn delete();
});


pub struct NMClient<'a> {
    client: Rc<dbus::Connection>,
    nm: NetworkManager<'a>,
    settings: Settings<'a>,
    properties: Properties<'a>,
}

impl<'a> NMClient<'a> {
    pub fn connect() -> errors::Result<Self> {
        let client = Rc::new(dbus::Connection::get_private(dbus::BusType::System)?);
        Ok(Self {
               client: client.clone(),
               nm: NetworkManager::new(SVC_NETWORK_MANAGER, PATH_NETWORK_MANAGER, client.clone()),
               settings: Settings::new(SVC_NETWORK_MANAGER, PATH_SETTINGS, client.clone()),
               properties: Properties::new(SVC_NETWORK_MANAGER,
                                           PATH_NETWORK_MANAGER,
                                           client.clone()),
           })
    }

    pub fn primary_connection(&self) -> errors::Result<Connection> {
        let active_connection_path = self.properties
            .get(IFC_NETWORK_MANAGER, PROP_PRIMARY_CONNECTION)?
            .as_str()
            .ok_or(errors::ErrorKind::DBusPropertyReadError(PROP_PRIMARY_CONNECTION))?
            .to_owned();
        let connection_path = Properties::new(SVC_NETWORK_MANAGER,
                                              active_connection_path,
                                              self.client.clone())
                .get(IFC_CONNECTION_ACTIVE, PROP_CONNECTION)?
                .as_str()
                .ok_or(errors::ErrorKind::DBusPropertyReadError(PROP_CONNECTION))?
                .to_owned();
        Ok(Connection::new(SVC_NETWORK_MANAGER, connection_path, self.client.clone()))
    }

    pub fn list_connections(&self) -> errors::Result<Vec<Connection>> {
        Ok(self.settings
               .list_connections()?
               .iter()
               .map(|path| {
                        Connection::new(SVC_NETWORK_MANAGER, path.clone(), self.client.clone())
                    })
               .collect())
    }

    pub fn device_path(&self,
                       device_type: DeviceType,
                       interface: Option<&str>)
                       -> errors::Result<Path> {
        for path in self.nm.get_devices()? {
            let prop = Properties::new(SVC_NETWORK_MANAGER, path.clone(), self.client.clone());
            let cur_type = prop.get(IFC_DEVICE, PROP_DEVICE_TYPE)?
                .as_i64()
                .ok_or(errors::ErrorKind::DBusPropertyReadError(PROP_DEVICE_TYPE))?;
            let cur_ifc = prop.get(IFC_DEVICE, PROP_INTERFACE)?
                .as_str()
                .ok_or(errors::ErrorKind::DBusPropertyReadError(PROP_INTERFACE))?
                .to_owned();
            if DeviceType::from_i64(cur_type) == Some(device_type) {
                if interface.map(|ifc| ifc == cur_ifc).unwrap_or(true) {
                    return Ok(path);
                }
            }
        }
        Err(errors::ErrorKind::NetworkDeviceNotFound.into())
    }

    pub fn add_and_activate_connection(&self,
                                       connection: ConnectionSettings,
                                       device: Path)
                                       -> errors::Result<Path> {
        let conn_path = self.settings.add_connection(connection)?;
        Ok(self.nm
               .activate_connection(conn_path, device, Path::new("/")?)?)
    }

    pub fn enable_wifi(&self, value: Variant) -> errors::Result<()> {
        self.properties.set(IFC_NETWORK_MANAGER, PROP_WIRELESS_ENABLED, value)?;
        thread::sleep(Duration::from_secs(1));
        Ok(())
    }

    pub fn monitor_state(&self) -> errors::Result<StateMonitor> {
        StateMonitor::new(&self.client)
    }
}


pub struct StateMonitor(DBusMatcher);

impl StateMonitor {
    fn new(client: &Rc<dbus::Connection>) -> errors::Result<Self> {
        Ok(StateMonitor(DBusMatcher::new(client, IFC_NETWORK_MANAGER, SIG_STATE_CHANGED)?))
    }

    pub fn wait<F>(&self, timeout_sec: u32, accept: F) -> Option<State>
        where F: Fn(State) -> bool
    {
        fn to_state(m: &dbus::Message) -> State {
            m.get1::<u32>()
                .and_then(State::from_u32)
                .unwrap_or(State::Unknown)
        }
        self.0
            .wait(timeout_sec, |m| accept(to_state(m)))
            .map(|m| {
                     m.get1::<u32>()
                         .and_then(State::from_u32)
                         .unwrap_or(State::Unknown)
                 })
    }
}

struct DBusMatcher {
    client: Rc<dbus::Connection>,
    interface: String,
    member: String,
}

impl DBusMatcher {
    fn new(client: &Rc<dbus::Connection>, interface: &str, member: &str) -> errors::Result<Self> {
        let res = Self {
            client: client.clone(),
            interface: String::from(interface),
            member: String::from(member),
        };
        client.add_match(&Self::mk_match(&res))?;
        Ok(res)
    }

    fn wait<F>(&self, timeout_sec: u32, accept: F) -> Option<dbus::Message>
        where F: Fn(&dbus::Message) -> bool
    {
        let start = Instant::now();
        let timeout = Duration::from_secs(timeout_sec as u64);
        for item in self.client.iter(200) {
            if let dbus::ConnectionItem::Signal(m) = item {
                if m.interface() == Some(self.interface.clone().into()) &&
                   m.member() == Some(self.member.clone().into()) && accept(&m) {
                    return Some(m);
                }
            }
            if start.elapsed() >= timeout {
                break;
            }
        }
        None
    }

    fn mk_match(&self) -> String {
        format!("interface='{}',member='{}'", self.interface, self.member)
    }
}

impl Drop for DBusMatcher {
    fn drop(&mut self) {
        let _ = self.client.remove_match(&self.mk_match());
    }
}
