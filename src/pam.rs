use std;
use libc;
use num_traits::FromPrimitive;
use ffi::ptr::*;
use errors::{Result, ErrorKind};

enum_from_primitive! {
#[allow(dead_code)]
#[derive(Debug, PartialEq)]
pub enum ResultCode {
    Success = 0,
    OpenErr = 1,
    SymbolErr = 2,
    ServiceErr = 3,
    SystemErr = 4,
    BufErr = 5,
    PermDenied = 6,
    AuthErr = 7,
    CredInsufficient = 8,
    AuthinfoUnavail = 9,
    UserUnknown = 10,
    Maxtries = 11,
    NewAuthtokReqd = 12,
    AcctExpired = 13,
    SessionErr = 14,
    CredUnavail = 15,
    CredExpired = 16,
    CredErr = 17,
    NoModuleData = 18,
    ConvErr = 19,
    AuthtokErr = 20,
    AuthtokRecoveryErr = 21,
    AuthtokLockBusy = 22,
    AuthtokDisableAging = 23,
    AuthtokTryAgain = 24,
    Ignore = 25,
    Abort = 26,
    AuthtokExpired = 27,
    ModuleUnknown = 28,
    BadItem = 29,
    ConvAgain = 30,
    Incomplete = 31,
}
}

#[allow(dead_code)]
#[derive(Debug, PartialEq)]
enum ItemType {
    Service = 1,
    User = 2,
    Tty = 3,
    Rhost = 4,
    Conv = 5,
    Authtok = 6,
    Oldauthtok = 7,
    Ruser = 8,
    UserPrompt = 9,
    FailDelay = 10,
    Xdisplay = 11,
    Xauthdata = 12,
    AuthtokType = 13,
}

fn pam_error<T>(res_code: ResultCode) -> Result<T> {
    Err(ErrorKind::PamError(res_code).into())
}

pub enum PamHandleT {}

#[link(name = "pam")]
extern "C" {
    fn pam_get_user(pamh: *const PamHandleT,
                    user: *mut *const libc::c_char,
                    prompt: *const libc::c_char)
                    -> libc::c_int;
    fn pam_get_authtok(pamh: *const PamHandleT,
                       item: libc::c_int,
                       authtok: *mut *const libc::c_char,
                       prompt: *const libc::c_char)
                       -> libc::c_int;
}

impl PamHandleT {
    pub fn get_user(&self) -> Result<String> {
        let mut ptr: *const libc::c_char = std::ptr::null();
        let res = ResultCode::from_i32(unsafe { pam_get_user(self, &mut ptr, std::ptr::null()) })
            .unwrap_or(ResultCode::ServiceErr);
        match res {
            ResultCode::Success => Ok(ptr_to_str(ptr)?),
            err_code => pam_error(err_code),
        }
    }

    pub fn get_password(&self) -> Result<String> {
        let mut ptr: *const libc::c_char = std::ptr::null();
        let res = ResultCode::from_i32(unsafe {
                                           pam_get_authtok(self,
                                                           ItemType::Authtok as libc::c_int,
                                                           &mut ptr,
                                                           std::ptr::null())
                                       })
                .unwrap_or(ResultCode::ServiceErr);
        match res {
            ResultCode::Success => Ok(ptr_to_str(ptr)?),
            err_code => pam_error(err_code),
        }
    }
}
