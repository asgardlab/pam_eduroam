use std::fs::File;
use std::io::prelude::*;
use std::collections::HashMap;
use toml;
use userdb::Uid;
use errors::Result;

const SETTINGS_PATH: &str = "/etc/pam_eduroam.toml";

#[derive(Debug, Deserialize)]
pub struct Settings {
    pub path: SettingsPath,
    pub user: SettingsUser,
    pub wireless: SettingsWireless,
    realm: Option<String>,
}

#[derive(Debug, Deserialize)]
pub struct SettingsPath {
    pub userdb: String,
    pub ca_cert_dir: String,
    eapconfig: String,
}

#[derive(Debug, Deserialize)]
pub struct SettingsUser {
    pub first_uid: Uid,
    pub shell: String,
    pub home_dir: String,
}

#[derive(Debug, Deserialize)]
pub struct SettingsWireless {
    pub device: Option<String>,
    pub ssid: String,
    pub timeout: u32,
}

#[derive(Debug, Deserialize, Clone)]
pub struct EAPConfig {
    pub eap: Vec<String>,
    pub phase2_auth: String,
    pub ca_cert: String,
    pub server_name: String,
    pub anonymous_identity: String,
}

fn read_contents(filename: &str) -> Result<String> {
    let mut file = File::open(filename)?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    return Ok(contents);
}

impl Settings {
    pub fn load() -> Result<Self> {
        Ok(toml::from_str(&read_contents(SETTINGS_PATH)?)?)
    }

    pub fn eap_map(&self) -> Result<HashMap<String, EAPConfig>> {
        Ok(toml::from_str(&read_contents(&self.path.eapconfig)?)?)
    }

    pub fn insert_realm(&self, account: String) -> String {
        match self.realm {
            Some(ref realm) if !account.contains("@") => format!("{}@{}", account, realm),
            _ => account,
        }
    }
}
